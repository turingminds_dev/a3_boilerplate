import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '../app/app.service';
import { Location } from '@angular/common';

declare const FB: any;

@Injectable()
export class LoginLogoutService {
    constructor(private appState: AppState, private router: Router) {
    }
    public Login(loginModel: any) {
        localStorage.setItem('JWT', JSON.stringify(loginModel.accessToken));
        this.appState.set('loggedIn', true);
        this.router.navigate(['/settings']);
    }
    public Logout() {
        localStorage.removeItem('JWT');
        this.appState.set('loggedIn', false);
        console.log('12')
        this.router.navigate(['/']);
    }
}

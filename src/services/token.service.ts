import { Inject, Injectable, Injector } from '@angular/core';
import {
    HttpClient, HttpHeaders, HttpParams,
    HttpRequest, HttpResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { AppState } from '../app/app.service';
import { apiUrl } from '../app/config/configuration';
import * as Util from '../shared/utils/utils';
import * as Config from '../app/config/configuration';
import 'rxjs/Rx';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TokenService {
    public authToken: any;
    private http: HttpClient;
    constructor(http: HttpClient, private router: Router, private appState: AppState) {
        this.http = http;
    }



    public getAuthToken(useAuth?: boolean) {
        let promise = new Promise((resolve, reject) => {
            let JWT = JSON.parse(localStorage.getItem('JWT'));
            if (JWT != null) {
                let token = JWT;
                let expired: boolean;
                expired = Util.expiredJwt(token.access_token);
                if (token && !expired) {
                    resolve(token.access_token);
                } else if (token && expired) {
                    console.log('token expired');
                    let currentUserName = localStorage.getItem('UserName');
                    let loginDetails = { data: { userName: currentUserName, cr: '', grantType: 'refreshtoken' } };
                    loginDetails.data.cr = token.refresh_token
                    return this.http.post(apiUrl.tokenServer, loginDetails).timeout(30000)
                        .toPromise()
                        .then((tokenresp) => {
                            if (tokenresp['value'].code === '999') {
                                let loginModel = { accessToken: { access_token: tokenresp['value'].access_token, refresh_token: tokenresp['value'].refresh_token } };
                                localStorage.setItem('JWT', JSON.stringify(loginModel.accessToken));
                            }
                            return resolve(JSON.parse(localStorage.getItem('JWT')).access_token);
                        })
                        .catch((tokenerr) => {
                            reject('problem');
                            this.appState.set('loggedIn', false);
                            localStorage.removeItem('JWT');
                            localStorage.removeItem('UserName');
                            //this.router.navigate(['/loginregister']);
                        });
                }
            } else if (useAuth) {
                reject('no JWt');
                console.log(reject('no JWt'))
                this.appState.set('loggedIn', false);
                //                 this.router.navigate(['/loginregister']);
            } else {
                resolve('');
            }
        });
        return promise;
    }
}


export const apiUrl = {
    serverUrl: '',
    authServer: '',
    tokenServer: '',
    constantUrl: '',
};
// ***dev */
apiUrl.serverUrl = 'http://192.168.0.113:5000/api/';
apiUrl.authServer = 'http://192.168.0.113:5001/api/auth';
apiUrl.tokenServer = 'http://192.168.0.116:8080/api/auth';
apiUrl.constantUrl = 'http://192.168.0.116:8080/api/';

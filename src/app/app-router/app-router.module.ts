import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../login/login.component';
import { AppComponent } from '../app.component';
// import { PlainComponent } from '../plain/plain.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authguard/authguard';
import { SettingsComponent } from '../settings/settings.component';
import { AuthorizationComponent } from '../settings/authorization/authorization.component';
import { ProfileComponent } from '../settings/profile/profile.component';
import { HomeComponent } from '../home/home.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  //{ path: 'login', component: LoginComponent, canActivate: [AuthGuard] },   
  // { path: 'plain', component: PlainComponent },
  {
    path: 'settings', component: SettingsComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' },
      { path: 'authorization', component: AuthorizationComponent },
      { path: 'profile', component: ProfileComponent }
    ]
  }
]



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }

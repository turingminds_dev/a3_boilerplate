import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormGroup, Validators, NgForm, FormArray } from '@angular/forms';
import { apiUrl } from '../../config/configuration';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Pipe, PipeTransform } from '@angular/core';
import { Authapirole, Module, Permissions, Postapi } from '../../model/authapirole.model';
import { Authapi } from '../../model/authapi.model';
import { AbstractControl } from '@angular/forms';
import * as _ from 'underscore';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {
  postApiRole: any;
  activeRole: number;
  setActiveRow: boolean;
  showEditoption: boolean;
  roleExist = [];
  moduleList: any;
  showRoleDiv: boolean;
  roleList: any;
  disableRoleDiv: boolean = false;
  roleData: any = {};
  roleformAction: string;
  isNewRoleFrom: boolean;
  module: any = {};
  permission: any = {};
  roleRoleList: any;
  @ViewChild('roleForm')
  roleForm: NgForm;
  constructor(
    private apiService: ApiService,
    public toastr: ToastsManager, vcr: ViewContainerRef
  ) {
    this.roleData = new Authapirole();
    this.module = new Module();
    this.permission = new Permissions();
    this.toastr.setRootViewContainerRef(vcr);
    this.disableRoleDiv = false;
    this.postApiRole = new Postapi();
  }

  ngOnInit() {
    this.apiService.get('', '', apiUrl.constantUrl + 'authorization/apiroles').then(
      (response: any) => {
        if (response.data === undefined) {
          throw response.error;
        }
        if (response.header.resCode === '200') {
          this.roleList = response.data;
          for (let i = 0; i < this.roleList.length; i++) {
            this.roleExist.push(this.roleList[i].roleName);
          }
        }
      })
      .catch(
      (error: any) => {
        console.log(error);
        if (error.code === '401') {
          this.toastr.error('Wrong Credentials. Please try again');
        }
        if (error.code === '404') {
          this.toastr.error('User not Found. Please register to continue.');
        }
      }
      );




    // this.apiService.get('', '', apiUrl.constantUrl + 'authorization/api').then(
    //   (response: any) => {
    //     if (response.data === undefined) {
    //       throw response.error;
    //     }
    //     if (response.header.resCode === '200') {
    //       this.moduleList = response.data[0];
    //       //this.toastr.success('Successfully Fetched Containertype', 'Success!');
    //     }
    //   })
    //   .catch(
    //   (error: any) => {
    //     console.log(error);
    //     // this.toastr.error('This is not good!', 'Oops!');
    //     if (error.code === '401') {
    //       this.toastr.error('Wrong Credentials. Please try again');
    //     }
    //     if (error.code === '404') {
    //       this.toastr.error('User not Found. Please register to continue.');
    //     }
    //   }
    //   );

  }

  addAndeditRole(role: Authapirole) {
    this.roleformAction = '';
    this.disableRoleDiv = false;
    this.showRoleDiv = true;
    this.setActiveRow = false;

    if (!role) {
      this.isNewRoleFrom = true;
      this.roleformAction = 'Add';
      this.emptyRole();
      this.showEditoption = false;
      this.activeRole = null;
      (<HTMLInputElement>document.getElementById("roletextbox")).focus();
      return;
    } else {
      this.activeRole = role.roleName;
      this.setActiveRow = true;
      this.disableRoleDiv = true;
      this.roleformAction = 'Edit';
      this.isNewRoleFrom = false;
      this.roleData = role;
      this.showEditoption = true;
    }
  }

  emptyRole() {
    this.roleData = new Authapirole();
    for (let j = 0; j < this.roleList[0].modules.length; j++) {
      this.roleData.modules.push({ "moduleName": this.roleList[0].modules[j].moduleName, "permissions": new Permissions() });
    }
  }




  cancelRole() {
    this.emptyRole();
  }


  saveRole(roleData) {
    this.postApiRole.roleId = roleData.roleId;
    for (let j = 0; j < this.roleData.modules.length; j++) {
      this.postApiRole.modules.push({ "moduleName": this.roleData.modules[j].moduleName, "permissions": this.roleData.modules[j].permissions });
    }
    //console.log(this.postApiRole);

    if (this.roleformAction === 'Edit') {
      this.apiService.put('', this.postApiRole, '', apiUrl.constantUrl + 'authorization/apiroles').then(
        (response: any) => {
          if (response.data === undefined) {
            throw response.error;
          }
          if (response.header.resCode === '200') {
            this.toastr.success('Successfully Updated', 'Success!');
            this.showRoleDiv = false;
          }
        })
        .catch(
        (error: any) => {
          if (error.header.resCode === '401') {
            this.toastr.error('Wrong Credentials. Please try again');
          }
          if (error.header.resCode === '404') {
            this.toastr.error('User not Found.');
          }
        }
        );
    }
    else if (this.roleformAction === 'Add') {
      this.apiService.post('', this.postApiRole, '', apiUrl.constantUrl + 'authorization/apiroles').then(
        (response: any) => {
          if (response.data === undefined) {
            throw response.error;
          }
          if (response.header.resCode === '200') {
            this.toastr.success('Successfully Added', 'Success!');
            this.showRoleDiv = false;
          }
        })
        .catch(
        (error: any) => {
          if (error.header.resCode === '401') {
            this.toastr.error('Wrong Credentials. Please try again');
          }
          if (error.header.resCode === '404') {
            this.toastr.error('User not Found.');
          }
        }
        );
    }
  }



  onRoleEdit() {
    this.disableRoleDiv = false;
  }

  checkRoleExist(roleName) {
    let typevalue = this.roleData.roleName;
    let checkValueExist = _.find(this.roleExist, function (list) {
      if (list === typevalue) {
        return true;
      }
    });
    if (checkValueExist === typevalue) {
      this.toastr.warning(' This Role Already Exist', 'exist!');
      for (let i = 0; i < this.roleList.length; i++) {
        if (this.roleList[i].roleName === roleName) {
          this.roleData = this.roleList[i];
          this.showEditoption = true;
          this.disableRoleDiv = true;
        }
      }
    }
  }
}

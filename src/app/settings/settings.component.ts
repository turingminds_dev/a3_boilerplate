import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  lastData: string;
  activeProfile: Boolean;
  activeAuthorized: boolean;

  constructor() {
    this.lastData = window.location.href.split('/').pop();
    if (this.lastData === 'authorization') {
      this.activeProfile = false;
      this.activeAuthorized = true;
    }
    else {
      this.activeProfile = true;
      this.activeAuthorized = false;
    }
  }

  ngOnInit() {
  }

  onProfile() {
    this.activeProfile = true;
    this.activeAuthorized = false;
  }

  onAuthorized() {
    this.activeProfile = false;
    this.activeAuthorized = true;
  }
}

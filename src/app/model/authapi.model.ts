export class Authapi {
    public apiId: number;
    public modules: string;
    public apiUrl: string;
    public method: string;
    constructor(apiId?: number, modules?: string, apiUrl?: string, method?: string) {
        this.apiId = apiId || null;
        this.modules = modules || null;
        this.apiUrl = apiUrl || null;
        this.method = method || null;
    }
}






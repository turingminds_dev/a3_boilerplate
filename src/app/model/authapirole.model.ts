export class Authapirole {
    public roleName: number;
    public modules: Module[];
    constructor(roleName?: number, modules?: Module[]) {
        this.roleName = roleName || null;
        this.modules = modules || [];
    }
}




export class Module {
    public moduleName: number;
    public url: string;
    public permissions: {
        Permissions
    };
    constructor(moduleName?: number, url?: string, permissions?: { Permissions }, method?: string) {
        this.moduleName = moduleName || null;
        this.url = url || null;
        this.permissions = permissions;
    }
}


export class Permissions {
    public get: boolean;
    public put: boolean;
    public post: boolean;

    constructor(get?: boolean, put?: boolean, post?: boolean) {
        this.get = get || false;
        this.post = post || false;
        this.put = put || false;
    }
}

export class Postapi {
    public roleId: number;
    public modules: Module[];

    constructor(roleId?: number, modules?: Module[]) {
        this.roleId = roleId || null;
        this.modules = modules || [];
    }
}







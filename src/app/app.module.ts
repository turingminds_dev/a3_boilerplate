import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginDirective } from './directive/login.directive';
import { AppRouterModule } from './app-router/app-router.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { AppState } from './app.service';
import { TokenService } from '../services/token.service';
import { LoginLogoutService } from '../services/loginlogout.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth/authguard/authguard';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { CustomOption } from './custom-option';
import { ToastOptions } from 'ng2-toastr';
import { SettingsComponent } from './settings/settings.component';
import { AuthorizationComponent } from './settings/authorization/authorization.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DatePipe } from '@angular/common';
import { ProfileComponent } from './settings/profile/profile.component';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { HomeComponent } from './home/home.component';
import { enableProdMode } from '@angular/core';
enableProdMode();

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginDirective,
    ResetpasswordComponent,
    SettingsComponent,
    AuthorizationComponent,
    ProfileComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NguiAutoCompleteModule

  ],
  providers: [AuthGuard, AppState, ApiService, TokenService, LoginLogoutService, DatePipe,
    { provide: ToastOptions, useClass: CustomOption }],
  bootstrap: [AppComponent]
})
export class AppModule { }

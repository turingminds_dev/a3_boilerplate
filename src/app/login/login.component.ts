import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { LoginLogoutService } from '../../services/loginlogout.service';
import { apiUrl } from '../config/configuration';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private apiService: ApiService,
    vcr: ViewContainerRef, private loginLogout: LoginLogoutService, public toastr: ToastsManager) {
    this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
    let loginDetails = { data: { userName: '', passWord: '', grantType: 'password' } };
    loginDetails.data.userName = form.value.username;
    loginDetails.data.passWord = form.value.password;
    this.apiService.post('', loginDetails, undefined, apiUrl.tokenServer).then(
      (response: any) => {
        if (response.value === undefined) {
          throw response.error;
        }
        if (response.value.code === '999') {
          let loginModel = { accessToken: { access_token: response.value.access_token, refresh_token: response.value.refresh_token } };
          localStorage.setItem('UserName', form.value.username);
          this.loginLogout.Login(loginModel);
          this.toastr.success('User Is loggedIn!', 'Success!');
        }
      })
      .catch(
      (error: any) => {
        console.log(error);
        if (error.header.resCode === '403') {
          this.toastr.error('invalid random number/token expired');
        }
        if (error.header.resCode === '404') {
          this.toastr.error('User not Found.');
        }
      }
      );
  }
}

import { Directive, OnDestroy, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appLogin]'
})
export class LoginDirective implements OnDestroy, AfterViewInit {

  constructor() { }


  ngAfterViewInit() {
    document.querySelector('body').classList.add('loginBody');

  }
  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('loginBody');
  }

}

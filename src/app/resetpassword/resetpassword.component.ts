import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { LoginLogoutService } from '../../services/loginlogout.service';
import { apiUrl } from '../config/configuration';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  username;

  constructor(private apiService: ApiService, private loginLogout: LoginLogoutService) { }

  ngOnInit() {
  }



  onReset() {
    this.apiService.post('', this.username, undefined, apiUrl.tokenServer).then(
      (response: any) => {
        if (response.value === undefined) {
          throw response.error;
        }
        if (response.value.code === '999') {
          console.log('success');
        }
      })
      .catch(
      (error: any) => {
        if (error.code === '401') {
          console.log('Wrong Credentials. Please try again');
        }
        if (error.code === '404') {
          console.log('User not Found. Please register to continue.');
        }
      }
      );
  }

}
